package model;

public class mdlTransactionLog {
    public String TransactionID; // varchar2(21) - Primary Key - auto increment ex : TRA-20180508-00000001
    public String SequenceID; // varchar(26) - sequence ID
    public String SerialNumber; // varchar2(50) can not be null
    public String WSID; // varchar2(50) - can not be null
    public String StartTime; // timestamp
    public String EndTime; // timestamp
    public String CustomerNumber; // varchar2(11)
    public String AccountNumber; // varchar2(11)
    public String LoginType; // varchar2(50) NIK atau ATM card
    public String LoginNumber; // varchar2(50) nomor KTP atau ATM Card
    public String BranchCode; // varchar2(50) dari device management
    public String TransactionType; // ganti buku, cetak buku.
    public String LogoutReason; // varchar2(50) success, auto logout
    public String VerifType1; // varchar2(50) verify type 1
    public String VerifNumber1; // varchar2(50) verify number 1
    public String VerifType2; // varchar2(50) verify type 2
    public String VerifNumber2; // varchar2(50) verify number 2
    public String Financial; // varchar2(100) flag financial ("MBCA", "KLIKBCA", "MBCA, KLIKBCA")
    public String ReferenceNumber; //varchar(50) reference number dari eForm untuk transaksi MBCA, KLIKBCA
    public String PrintPageCount; //integer jumlah berapa halaman dicetak jika transaksi tersebut adalah cetak buku
    public String AccountType; // varchar(50) tipe rekening nasabah
    public String Trailer1; // varchar(50) penambahan field untuk keterangan
    public String Trailer2; // varchar(50) penambahan field untuk keterangan
}
