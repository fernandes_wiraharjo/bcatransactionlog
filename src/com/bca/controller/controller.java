package com.bca.controller;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import adapter.LogAdapter;
import adapter.TransactionLogAdapter;

@RestController
public class controller {
	final static Logger logger = LogManager.getLogger(controller.class);
	static String apiName = "BCATransactionLog";
	static String apiMethod = "POST";

	@RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
	public @ResponseBody String GetPing() {
		String ConnectionStatus = "true";
		return ConnectionStatus;
	}

	@RequestMapping(value = "/log", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
	public @ResponseBody model.mdlAPIResult TransactionLog(@RequestBody model.mdlTransactionLog param) {
		long startTime = System.currentTimeMillis();
		String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
		model.mdlAPIResult mdlTransactionLogResult = new model.mdlAPIResult();
		model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
		model.mdlMessage mdlMessage = new model.mdlMessage();
		model.mdlResult mdlResult = new model.mdlResult();
		Gson gson = new Gson();

		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.WSID = param.WSID;
		mdlLog.SerialNumber = param.SerialNumber;
		mdlLog.ApiFunction = "TransactionLog";
		mdlLog.SystemFunction = "TransactionLog";
		mdlLog.LogSource = "Webservice";
		mdlLog.LogStatus = "Failed";
		mdlResult.Result = "false";

		String transactionID = "";
		Boolean isInsert = true;

		try {
			// if there is no transactionID, then insert new log with transactionID as return result
			if (param.TransactionID == null || param.TransactionID.equalsIgnoreCase("")) {
				// check if there are transactions of the WSID that have StartTime but no EndTime then update LogoutReason as ERROR.
				TransactionLogAdapter.CheckLastTransaction(param.WSID);
				// insert new transactionLog record if there is no TransactionID
				transactionID = TransactionLogAdapter.InsertTransactionLog(param);
			} else {
				// update existing transactionLog record based on the TransactionID
				transactionID = TransactionLogAdapter.UpdateTransactionLog(param);
				isInsert = false;
			}

			if (!transactionID.equalsIgnoreCase("") || (transactionID.equalsIgnoreCase("") && isInsert == false)) {
				mdlErrorSchema.ErrorCode = "00";
				mdlMessage.Indonesian = "Log berhasil disimpan";
				mdlMessage.English = "Log successfully saved";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlTransactionLogResult.ErrorSchema = mdlErrorSchema;
				mdlResult.Result = transactionID;
				mdlTransactionLogResult.OutputSchema = mdlResult;
				mdlLog.LogStatus = "Success";
				logger.info(LogAdapter.logControllerToLog4j(true, startTime, 200, apiName, apiMethod, "function: " + functionName, gson.toJson(param), gson.toJson(mdlTransactionLogResult)));
			} else {
				mdlErrorSchema.ErrorCode = "01";
				mdlMessage.Indonesian = "Log gagal disimpan";
				mdlMessage.English = mdlLog.ErrorMessage = "Save log failed";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlTransactionLogResult.ErrorSchema = mdlErrorSchema;
				mdlTransactionLogResult.OutputSchema = mdlResult;
				logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiName, apiMethod, "function: " + functionName, gson.toJson(param), gson.toJson(mdlTransactionLogResult)));
			}
		} catch (Exception ex) {
			mdlErrorSchema.ErrorCode = "02";
			mdlMessage.Indonesian = "Gagal memanggil service";
			mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
			mdlErrorSchema.ErrorMessage = mdlMessage;
			mdlTransactionLogResult.ErrorSchema = mdlErrorSchema;
			mdlTransactionLogResult.OutputSchema = mdlResult;
			logger.error(LogAdapter.logControllerToLog4jException(startTime, 500, apiName, apiMethod, "function: " + functionName, gson.toJson(param), gson.toJson(mdlTransactionLogResult), ex.toString()), ex);
		}
		LogAdapter.InsertLog(mdlLog);
		return mdlTransactionLogResult;
	}
}
