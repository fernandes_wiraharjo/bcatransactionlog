package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import com.google.gson.Gson;

public class TransactionLogAdapter {
    final static Logger logger = LogManager.getLogger(TransactionLogAdapter.class);

    public static String InsertTransactionLog(model.mdlTransactionLog mdlTransactionLog) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	String transactionID = "";
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	Gson gson = new Gson();
	try {
	    String newTransactionID = CreateTransactionID(mdlTransactionLog.WSID);
	    String dateNow = LocalDateTime.now().toString().replace("T", " ");
	    String startTimeTransaction = mdlTransactionLog.StartTime == null || mdlTransactionLog.StartTime.equals("") ? dateNow : mdlTransactionLog.StartTime;
	    String endTime = mdlTransactionLog.EndTime == null || mdlTransactionLog.EndTime.equals("") ? "" : mdlTransactionLog.EndTime;
	    // define connection
	    connection = database.RowSetAdapter.getConnectionWL();
	    String sql = "INSERT INTO TransactionLog (TransactionID, SerialNumber, WSID, StartTime, EndTime, "
		    + "CustomerNumber, AccountNumber, LoginType, LoginNumber, BranchCode, "
		    + "TransactionType, LogoutReason, CreatedDate, SequenceID)"
		    + "VALUES (?,?,?,TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF'),TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF'),"
		    + "?,?,?,?,?,"
		    + "?,?,TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF'),?)";

	    pstm = connection.prepareStatement(sql);

	    // insert sql parameter
	    pstm.setString(1, newTransactionID);
	    pstm.setString(2, mdlTransactionLog.SerialNumber);
	    pstm.setString(3, mdlTransactionLog.WSID);
	    pstm.setString(4, startTimeTransaction);
	    pstm.setString(5, endTime);
	    pstm.setString(6, mdlTransactionLog.CustomerNumber);
	    pstm.setString(7, mdlTransactionLog.AccountNumber);
	    pstm.setString(8, mdlTransactionLog.LoginType);
	    pstm.setString(9, mdlTransactionLog.LoginNumber);
	    pstm.setString(10, mdlTransactionLog.BranchCode);
	    pstm.setString(11, mdlTransactionLog.TransactionType);
	    pstm.setString(12, mdlTransactionLog.LogoutReason);
	    pstm.setString(13, dateNow);
	    pstm.setString(14, mdlTransactionLog.SequenceID);

	    // execute query
	    jrs = pstm.executeQuery();
	    transactionID = newTransactionID;
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCATransactionLog", "POST", "function: " + functionName + ", dataLog : "
		    + gson.toJson(mdlTransactionLog), "", "", ex.toString()), ex);
	} finally {
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception ex) {
		logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCATransactionLog", "POST", "function: " + functionName + ", dataLog : "
			+ gson.toJson(mdlTransactionLog), "", "", ex.toString()), ex);
	    }
	}
	return transactionID;
    }

    public static String UpdateTransactionLog(model.mdlTransactionLog mdlTransactionLog) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	String transactionID = "";
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	Gson gson = new Gson();
	try {
	    String dateNow = LocalDateTime.now().toString().replace("T", " ");
	    String endTime = mdlTransactionLog.EndTime == null || mdlTransactionLog.EndTime.equals("") ? dateNow : mdlTransactionLog.EndTime;
	    String verifType1 = mdlTransactionLog.VerifType1 == null ? "" : mdlTransactionLog.VerifType1;
	    String verifNumber1 = mdlTransactionLog.VerifNumber1 == null ? "" : mdlTransactionLog.VerifNumber1;
	    String verifType2 = mdlTransactionLog.VerifType2 == null ? "" : mdlTransactionLog.VerifType2;
	    String verifNumber2 = mdlTransactionLog.VerifNumber2 == null ? "" : mdlTransactionLog.VerifNumber2;
	    String financial = mdlTransactionLog.Financial == null ? "" : mdlTransactionLog.Financial;
	    String referenceNumber = mdlTransactionLog.ReferenceNumber == null ? "" : mdlTransactionLog.ReferenceNumber;
	    String printPageCount = mdlTransactionLog.PrintPageCount == null ? "0" : mdlTransactionLog.PrintPageCount;
	    String accountType = mdlTransactionLog.AccountType == null ? "" : mdlTransactionLog.AccountType;
	    String trailer1 = mdlTransactionLog.Trailer1 == null ? "" : mdlTransactionLog.Trailer1;
	    String trailer2 = mdlTransactionLog.Trailer2 == null ? "" : mdlTransactionLog.Trailer2;
	    int pageCount = 0;
	    try {
		pageCount = Integer.parseInt(printPageCount);
	    } catch (Exception e) {

	    }

	    // define connection
	    connection = database.RowSetAdapter.getConnectionWL();
	    String sql = "UPDATE TransactionLog "
		    + "SET SerialNumber = ?, WSID = ?, EndTime = TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF'), CustomerNumber = ?, AccountNumber = ?, "
		    + "LoginType = ?, LoginNumber = ?, BranchCode = ?, TransactionType = ?, LogoutReason = ?, "
		    + "UpdatedDate = TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF'), SequenceID = ?, VerifType1 = ?, VerifNumber1 = ?, VerifType2 = ?, "
		    + "VerifNumber2 = ?, Financial = ?, ReferenceNumber = ?, PrintPageCount = ?, AccountType = ?, "
		    + "Trailer1 = ?, Trailer2 = ? "
		    + "WHERE TransactionID = ?";
	    pstm = connection.prepareStatement(sql);

	    // insert sql parameter
	    pstm.setString(1, mdlTransactionLog.SerialNumber);
	    pstm.setString(2, mdlTransactionLog.WSID);
	    pstm.setString(3, endTime);
	    pstm.setString(4, mdlTransactionLog.CustomerNumber);
	    pstm.setString(5, mdlTransactionLog.AccountNumber);
	    pstm.setString(6, mdlTransactionLog.LoginType);
	    pstm.setString(7, mdlTransactionLog.LoginNumber);
	    pstm.setString(8, mdlTransactionLog.BranchCode);
	    pstm.setString(9, mdlTransactionLog.TransactionType);
	    pstm.setString(10, mdlTransactionLog.LogoutReason);
	    pstm.setString(11, dateNow);
	    pstm.setString(12, mdlTransactionLog.SequenceID);
	    pstm.setString(13, verifType1);
	    pstm.setString(14, verifNumber1);
	    pstm.setString(15, verifType2);
	    pstm.setString(16, verifNumber2);
	    pstm.setString(17, financial);
	    pstm.setString(18, referenceNumber);
	    pstm.setInt(19, pageCount);
	    pstm.setString(20, accountType);
	    pstm.setString(21, trailer1);
	    pstm.setString(22, trailer2);
	    pstm.setString(23, mdlTransactionLog.TransactionID);

	    // execute query
	    jrs = pstm.executeQuery();
	    // transactionID = mdlTransactionLog.TransactionID;
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCATransactionLog", "POST", "function: " + functionName + ", dataLog : "
		    + gson.toJson(mdlTransactionLog), "", "", ex.toString()), ex);
	} finally {
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception ex) {
		logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCATransactionLog", "POST", "function: " + functionName + ", dataLog : "
			+ gson.toJson(mdlTransactionLog), "", "", ex.toString()), ex);
	    }
	}
	return transactionID;
    }

    public static String CreateTransactionID(String WSID) {
	String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
	String transactionDate = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
	String lastTransactionID = GetLastTransactionID(WSID, transactionDate);
	String stringIncrement = "00000001";
	String transactionWSID = WSID == null || WSID.equals("") ? "00000000" : WSID;

	if ((lastTransactionID != null) && (!lastTransactionID.equals(""))) {
	    String[] partsTransactionID = lastTransactionID.split("-");
	    // get datetime and last running number log for the related WSID
	    String partDate = partsTransactionID[2];
	    String partNumber = partsTransactionID[3];

	    // check if the date is same, if same, add 1 to the number
	    if (partDate.equals(transactionDate)) {
		int inc = Integer.parseInt(partNumber) + 1;
		stringIncrement = String.format("%08d", inc);
	    }
	}
	StringBuilder sb = new StringBuilder();
	sb.append("TRA-").append(transactionWSID).append("-").append(transactionDate).append("-").append(stringIncrement);
	String TransactionID = sb.toString();
	return TransactionID;
    }

    public static String GetLastTransactionID(String WSID, String date) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	String TransactionID = "";
	try {
	    connection = database.RowSetAdapter.getConnectionWL();
	    pstm = connection.prepareStatement("SELECT NVL(MAX(TransactionID),'') as TransactionID FROM TransactionLog WHERE WSID = ? "
		    + "AND SUBSTR(TransactionID,14,8) = ? ");
	    pstm.setString(1, WSID);
	    pstm.setString(2, date);

	    jrs = pstm.executeQuery();

	    while (jrs.next()) {
		TransactionID = jrs.getString("TransactionID");
	    }
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCATransactionLog", "POST", "function: " + functionName, "", "", ex.toString()), ex);
	} finally {
	    // close the opened connection
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception ex) {
		logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCATransactionLog", "POST", "function: "
			+ functionName, "", "", ex.toString()), ex);
	    }
	}
	return TransactionID;
    }

    public static void CheckLastTransaction(String WSID) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;

	String dateNow = LocalDateTime.now().toString().replace("T", " ");
	try {
	    connection = database.RowSetAdapter.getConnectionWL();
	    pstm = connection.prepareStatement("UPDATE TransactionLog SET LogoutReason = 'FORCED', "
		    + "UpdatedDate = TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF') "
		    + "WHERE WSID = ? AND StartTime IS NOT NULL AND EndTime IS NULL AND LogoutReason IS NULL");
	    pstm.setString(1, dateNow);
	    pstm.setString(2, WSID);

	    jrs = pstm.executeQuery();
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCATransactionLog", "POST", "function: " + functionName, "", "", ex.toString()), ex);
	} finally {
	    // close the opened connection
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception ex) {
		logger.error(LogAdapter.logToLog4jException(startTime, 500, "BCATransactionLog", "POST", "function: "
			+ functionName, "", "", ex.toString()), ex);
	    }
	}
    }

}
